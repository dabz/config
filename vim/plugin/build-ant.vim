
""
"" build-ant.vim for Project in /home/gaspar_d
""
"" Made by Gasparina Damien
"" Login   <gaspar_d@epita.fr>
""
"" Started on  Wed 13 Apr 2011 05:07:29 PM CEST Gasparina Damien
"" Last update Wed 13 Apr 2011 05:07:29 PM CEST Gasparina Damien
""


au BufNewFile build.xml silent call AntBuild_New()


function AntBuild_New()
  $r ~/.vim/template/build.tpl
  call ReplaceFields('<!--', '  ', '-->')
  normal Gdh

  call Bind()
endfun


function Bind()

endfun
