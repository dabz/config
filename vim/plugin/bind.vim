"#############################################################################
"
"       Filename:  bind.vim
"
"    Description:  Epita coding style.
"
"   GVIM Version:  7.0+
"
"         Author:  Gasparina Damien
"          Email:  gaspar_d@epita.fr
"
"        Version:  see variable  g:BIND_Version  below
"        Created:  03.07.2010
"
"-----------------------------------------------------------------------------

if v:version < 700
  echomhl WarningMsg
  echom 'The plugin bind.vim needs Vim version >= 7 .'
  echomhl None
  finish
endif


" Prevent duplicate loading     {{{1
"
if exists("g:BIND_Version") || &cp
  finish
endif
let g:EPIC_Version= "0.1"


" Map key			{{{1
"
map <Space><Space> :FufBuffer <CR>
map ns :NERDTree <CR>
map nc :NERDTreeClose <CR>
map to :TlistOpen <CR>
map tc :TlistClose <CR>
map tr :TlistAddFilesRecursive .<CR>
map hh :A <CR>
map <F12> :!ctags -R --c++-kinds=+p --fields=+iaS --extra=+q .<CR>
map mm :%s/\([^ ]\)(/\0 (/ge <CR>
map <C-x><C-s> :w <CR>
