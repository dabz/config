""
"" c.vim for Project in /home/Danz/config
""
"" Made by Gasparina Damien
"" Login   <gaspar_d@epita.fr>
""
"" Started on  Sun 04 Jul 2010 09:49:49 PM CST Gasparina Damien
"" Last update Mon 15 Nov 2010 03:33:14 PM CET Gasparina Damien
""

" OmniCppComplete initialization
call omni#cpp#complete#Init()
