/*
** foo.hh for foo.hh
**
** Made by Gasparina Damien
** Login   gaspar_d <gaspar_d@epita.fr>
**
** Started on  Mon 29 Nov 2010 07:06:47 PM CET Gasparina Damien
** Last update Mon 29 Nov 2010 07:08:25 PM CET Gasparina Damien
*/


#ifndef FOO_HH_
# define FOO_HH_

class foo {
public:
  foo (int x);

  int bar(int x);

  virtual ~foo ();

private:
  int y;
};


#endif /* !FOO_HH_ */
