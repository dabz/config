/*
** lol.cc for lol.cc
**
** Made by Gasparina Damien
** Login   gaspar_d <gaspar_d@epita.fr>
**
** Started on  Mon 29 Nov 2010 06:58:22 PM CET Gasparina Damien
** Last update Mon 29 Nov 2010 07:02:17 PM CET Gasparina Damien
*/




// =====================================================================================
//        Class:  Bdfbdfbe
//  Description:
// =====================================================================================
class Bdfbdfbe
{
public:

  // ====================  LIFECYCLE     =======================================
  Bdfbdfbe ();                             // constructor
  Bdfbdfbe (int lol);                             // constructor
  Bdfbdfbe ( const Bdfbdfbe &other );   // copy constructor
  ~Bdfbdfbe ();                            // destructor

  int	foo();


protected:
  // ====================  DATA MEMBERS  =======================================

private:
  // ====================  DATA MEMBERS  =======================================

}; // -----  end of class Bdfbdfbe  -----




